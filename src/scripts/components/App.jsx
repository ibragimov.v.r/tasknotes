import React from 'react'
import Notes from './Notes'
import Note from "./Note"
import Popup from './Popup'
import createNote from '@scripts/store/actionCreators/createNote'
import {connect} from "react-redux";

const CreateNote = connect(null, {createNote})(Note)

class App extends React.Component {
    render() {
        return (
            <div className='app'>
                <h2>Notes:</h2>
                <Notes/>
                <Popup
                    popupName='Create note'
                    openButton={{
                        element: 'button', props: {className: 'mt-10 oButton'}, children: [
                            <img key={1} className='oButtonInside' src='/icons/plus.svg' alt=''/>
                        ]
                    }}
                    content={
                        <CreateNote header='new note' tasks={[]} edit={true} new={true}
                                    index={this.props.maxNoteId + 1}/>
                    }
                />
            </div>
        );
    }
}

const mapStateToProps = state => {
    let maxId = state.notes && state.notes.length > 0 ? Math.max(...state.notes.map(note => note.id)) : 0
    return {
        maxNoteId: maxId
    }
}

export default connect(mapStateToProps, null)(App)

