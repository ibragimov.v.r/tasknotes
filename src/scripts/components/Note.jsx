import React from 'react'
import PropTypes from 'prop-types'
import Tasks from "./Tasks"
import {createTask} from "@scripts/functions/objectCreators"
import Popup from "@scripts/components/Popup"

const DeleteContent = ({onClick, closePopup}) => {
    return (<input
        type='button'
        value={'Delete note'}
        className='inline ml-15'
        onClick={(event) => {
            onClick(event)
            closePopup ? closePopup() : null
        }}
        name='delete'
    />)
}

class Note extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            header: this.props.header,
            tasks: this.props.tasks ? this.props.tasks : new Map(),
            nameEdit: false
        }
        this.addTask = this.addTask.bind(this)
        this.handleDomEvent = this.handleDomEvent.bind(this)
        this.handleChangeTask = this.handleChangeTask.bind(this)
    }

    addTask() {
        const maxId = this.state.tasks && this.state.tasks.length > 0 ? Math.max(...this.state.tasks?.map(task => task.id)) : 0
        this.setState({
            ...this.state, tasks: this.state.tasks.concat(
                createTask(maxId + 1)
            )
        })
    }

    handleDomEvent(event) {
        const name = event.target?.name
        const newHeader = event.target.parentElement.firstElementChild?.value;
        switch (name) {
            case 'nameEdit' :
                this.setState(
                    {
                        ...this.state,
                        nameEdit: !this.state.nameEdit,
                        header: this.state.nameEdit ? newHeader : this.state.header
                    })
                break
            case 'delete' :
                this.props.closePopup ? this.props.closePopup() : null
                this.props.deleteNote({
                    id: this.props.index
                })
                break
            case 'save' :
                if (this.props.new) {
                    this.props.closePopup ? this.props.closePopup() : null
                    this.props.createNote({
                        id: this.props.index,
                        header: this.state.header,
                        tasks: this.state.tasks
                    })
                } else if (this.props.edit) {
                    this.props.closePopup ? this.props.closePopup() : null
                    this.props.editNote({
                        id: this.props.index,
                        header: this.state.header,
                        tasks: this.state.tasks
                    })
                }
        }
    }

    handleChangeTask(task) {
        const newTasks = task.checked !== undefined && task.text !== undefined
            ? this.state.tasks.filter(stask => stask.id !== task.id).concat(
                {...this.state.tasks.find(stask => stask.id === task.id), ...task}
            ).sort((task1, task2) => task1.id - task2.id)
            : this.state.tasks.filter(stask => stask.id !== task.id)
        this.setState({...this.state, tasks: newTasks})
    }

    render() {
        return (
            <div className='note mt-10 bb-1 p-5'>
                <div className='noteHeader'>
                    {this.props.edit
                        ? <form>
                            {this.state.nameEdit
                                ? <input type='text' defaultValue={this.state.header}/>
                                : <h3 className='inline'>{this.state.header}</h3>
                            }
                            <input
                                type='button'
                                value={!this.state.nameEdit ? 'Edit handler' : 'Save'}
                                className='inline ml-15'
                                onClick={this.handleDomEvent}
                                name='nameEdit'
                            />
                            {!this.props.new ? <Popup
                                popupName='delete note?'
                                openButton={{
                                    element: 'input',
                                    props: {
                                        type: 'button',
                                        className: 'inline ml-15',
                                        value: 'Delete note',
                                    }
                                }}
                                content={<DeleteContent onClick={this.handleDomEvent}/>}
                                stylePopup={{
                                    width: '130px',
                                    height: '50px',
                                    borderRadius: 'unset',
                                }}
                            /> : null}

                        </form>
                        : <div>
                            <h3 className='inline'>{this.props.header}</h3>
                            <Popup
                                popupName='Edit note'
                                openButton={{
                                    element: 'img',
                                    props: {
                                        src: '/icons/pen.svg',
                                        className: 'taskButton',
                                        alt: '',
                                        style: {width: '20px', marginLeft: '15px'}
                                    }
                                }}
                                content={
                                    <Note {...this.props} edit={true} new={false}/>
                                }
                            />
                        </div>
                    }
                </div>

                {this.props.edit
                    ? <div>
                        <Tasks tasks={this.state.tasks} edit={this.props.edit} onChange={this.handleChangeTask}/>
                        <button className='mt-10 oButton' onClick={this.addTask}>
                            <img className='oButtonInside' src='/icons/plus.svg' alt=''/>
                        </button>
                        <button className='saveButton buttonText textButton' name='save'
                                onClick={this.handleDomEvent}>Save
                        </button>
                    </div>
                    : <div className='noteBody'>
                        <Tasks tasks={this.props.tasks}/>
                    </div>
                }
            </div>
        )
    }
}

Note.propTypes = {
    header: PropTypes.string.isRequired,
    tasks: PropTypes.array.isRequired
}

export default Note