import React from 'react'
import Note from "./Note"
import {connect} from 'react-redux'
import editNote from "@scripts/store/actionCreators/editNote";
import deleteNote from "@scripts/store/actionCreators/deleteNote";

const NoteForEdit = connect(null, {editNote, deleteNote})(Note)

const Notes = ({notesArray}) => {
    return notesArray.map((note, index) => <NoteForEdit index={note.id} key={note.id} header={note.header} tasks={note.tasks}/>)
}


const mapStateToProps = state => {
    return {
        notesArray: state.notes
    }
}

export default connect(mapStateToProps, null)(Notes)