import React, { useState } from 'react'
import PropTypes from "prop-types";

class Popup extends React.Component {
    state = {
        isOpen: false
    }
    render() {
        return (
            <React.Fragment>
                <this.props.openButton.element {...this.props.openButton.props} onClick={() => {
                        this.setState({isOpen: true})
                }}>
                    {this.props.openButton.children || null}
                </this.props.openButton.element>
                {this.state.isOpen && <div className='popup'>
                    <div className='popup_inner' style={this.props.stylePopup}>
                        <div className='popupHeader'>
                        <span>
                            {this.props.popupName}
                        </span>
                            <input className='popupCloseButton' type='button' value='X' onClick={() => {
                                this.setState({isOpen: false})
                            }}/>
                        </div>
                        <div className='popupContent'>

                            {this.props.content ? (<this.props.content.type {...this.props.content.props} closePopup={() => {
                                this.setState({isOpen: false})
                            }}/>) : null}
                        </div>
                    </div>
                </div>}
            </React.Fragment>
        )
    }
}

Popup.propTypes = {
    popupName: PropTypes.string.isRequired,
    openButton: PropTypes.object.isRequired,
}

export default Popup