import React from 'react'
import PropTypes from "prop-types";

export default class Task extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            id: this.props.index,
            checked: this.props.checked,
            text: this.props.text,
            editText: false
        }

        this.handleDomEvent = this.handleDomEvent.bind(this)
    }

    setStateValue(name,value){
        this.setState({...this.state, [name]: value})
    }

    handleDomEvent(event){
        const name = event.target?.name
        switch (name) {
            case 'checked' :
                this.props.onChange ? this.props.onChange({
                    id: this.props.index,
                    checked: !this.state.checked,
                    text: this.state.text,
                }) : null
                this.setStateValue(name, !this.state.checked)
                break
            case 'editText' :
                if(this.state.editText && this.props.onChange){
                    this.props.onChange({
                        id: this.props.index,
                        checked: this.state.checked,
                        text: this.state.text,
                    })
                }
                this.setStateValue(name, !this.state.editText)
                break
            case 'delete' :
                this.props.onChange ? this.props.onChange({
                    id: this.props.index
                }) : null
                break
            case 'text' : this.setStateValue(name, event.target.value)
        }
    }

    render() {
        return (
            <div className='task p-5 bb-1 mt-5 flexLineContainer'>
                <div className=''>
                    <input
                        type='checkbox'
                        checked={this.props.edit ? this.state.checked : this.props.checked}
                        onChange={this.handleDomEvent}
                        disabled={!this.props.edit}
                        name='checked'
                    /><br/>
                    <img className='taskButton' src={`/icons/${this.state.editText ? 'save' : 'pen'}.svg`}
                         onClick={this.handleDomEvent} alt={''} hidden={!this.props.edit} name='editText'/><br/>
                    <img className='taskButton' src={`/icons/trash.svg`} onClick={this.handleDomEvent} alt={''}
                         hidden={!this.props.edit} name='delete'/><br/>
                </div>
                {this.props.edit
                    ? <textarea className='flexLineItem' rows={4} defaultValue={this.state.text} disabled={!this.state.editText} onChange={this.handleDomEvent} name='text'/>
                    : <span className='taskText flexLineItem'>{this.props.text}</span>
                }
            </div>
        )
    }
}

Task.propTypes = {
    checked: PropTypes.bool.isRequired,
    text: PropTypes.string.isRequired
}