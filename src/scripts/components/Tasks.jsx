import React from 'react'
import Task from "./Task"

export default ({tasks, edit, onChange}) => {
    return tasks.map((task, index) => <Task
        key={task.id}
        checked={task.checked}
        text={task.text}
        edit={edit}
        index={task.id}
        onChange={onChange}
    />)
}
