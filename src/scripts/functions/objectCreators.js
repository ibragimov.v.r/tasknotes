export const createTask = (id, text = 'new task') => {
    return {
        id: id,
        checked: false,
        text: text
    }
}