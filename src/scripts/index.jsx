import '@css/style.css'
import React from 'react'
import {render} from 'react-dom'
import App from './components/App'
import {compose, createStore} from "redux";
import {rootReducer} from "@scripts/store/reducers/rootReducer"
import {Provider} from "react-redux"


const store = createStore(rootReducer, compose(
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
))

render(<Provider store={store}>
    <App/>
</Provider>, document.getElementById('app'))




