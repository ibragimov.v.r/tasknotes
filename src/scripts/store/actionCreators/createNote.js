import {CREATE_NOTE} from "@scripts/store/types";

const createNote = (note) => {
    return {
        type: CREATE_NOTE,
        payload: note
    }
}
export default createNote