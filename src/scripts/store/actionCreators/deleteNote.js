import {DELETE_NOTE} from "@scripts/store/types";

const deleteNote = (note) => {
    return {
        type: DELETE_NOTE,
        payload: note
    }
}
export default deleteNote