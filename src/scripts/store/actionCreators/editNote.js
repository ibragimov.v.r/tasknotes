import {EDIT_NOTE} from "@scripts/store/types";

const editNote = (note) => {
    return {
        type: EDIT_NOTE,
        payload: note
    }
}
export default editNote