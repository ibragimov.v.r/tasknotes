import {CREATE_NOTE, DELETE_NOTE, EDIT_NOTE} from "@scripts/store/types"

const defaultState = JSON.parse(localStorage.getItem('notes')) || []

export const noteReducer = (state = defaultState, action) => {
    let newState;
    switch (action.type) {
        case CREATE_NOTE:
            newState = state.concat([action.payload])
            window.localStorage?.setItem('notes', JSON.stringify(newState))
            return state.concat([action.payload])
        case EDIT_NOTE:

            const newNote = action.payload
            newState = state.filter(note => note.id !== newNote.id).concat([newNote])
                .sort((note1, note2) => note1.id - note2.id)
            window.localStorage?.setItem('notes', JSON.stringify(newState))
            return newState
        case DELETE_NOTE:
            newState = state.filter(note => note.id !== action.payload.id)
            window.localStorage?.setItem('notes', JSON.stringify(newState))
            return newState
        default:
            return state
    }
}