import {combineReducers} from "redux";
import {noteReducer} from "@scripts/store/reducers/noteReducer";

export const rootReducer = combineReducers({
    notes: noteReducer
})